grey000.pgm             space
grey001.pgm             .  .  .  .  .
grey002.pgm             .. .. .. . . . .. . . . . . . .. . . ... .
grey003.pgm             ...............
grey004.pgm             ..:.:.:...:....:.:..:.:......: 
grey005.pgm             ::.::.::.::.::.::.::.::.::.::.::.::
grey006.pgm             ::::::::::::::
grey007.pgm             ;;:;:;:;:;;;;:;;:;:;:;:;;;:;:;:;
grey008.pgm             ;;;;;;;;;;;;;;;;;;;;;;;;;
                        ;=;=;=;=;=;=;=;=;=;=;=;=;
grey009.pgm             ====================
grey010.pgm             ++++++=+++=++++=+++++++=
grey011.pgm             |++|++|++|++|++|++|++|++|++
                        +++++++++++++++++++++++++++
grey012.pgm             |+|+|+|+|+|+|+|+||+|+||+|+|+|+|+||+|
grey013.pgm             ||+|||+||+|||+||||||+|||||+|||||+|||||+|
                        |||||||||||||||+||+|||||+|||||+|||||+|||
                        |||+||+||+||+||||+|||+|+|||+|+|||+|+|||+
                        ||+|||||+|||||+|||||||||||||||||||||||||
grey014.pgm             |i|i||i|i|||i|i|i||||i|||i|||i||i||i|
grey015.pgm             |iiii|ii|i|iii|iii|i|iii|iiii|ii|i|iii|iii
grey016.pgm             ::::::::::::::::::::::::
grey017.pgm             ll:ll:ll:ll:ll:l:lll:lll:lll:lll:lll:lll:
                        l:l:ll:l:l:l:ll:l:l:ll:l:l:ll:l:ll:l:l:ll
                        ll:ll:lllll:ll:lll:l:l:lll:l:ll:l:llll:l:
grey018.pgm             vIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIv
                        IIlIlIlIlIlIlIlIlIlIlIlIlIlIlIlIlIlIlIlIl
                        vlvIIvlvIvlvIvlvIvlvIvlvIvlvIvlvIvlvIvlvI
                        IIIIIIIIlIIIlIIIlIIIlIIIlIIIlIIIlIIIlIIIl
                        vlvlvlIvlvlvlvlvlvlvlvlvlvlvlvlvlvlvlvlvl
                        IIIIlIvlIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
                        vlvlvlIIIIvlvlvlvlvlvlvlvlvlvlvlvlvlvlvlv
grey019.pgm             IvvIvvIvIvvIvvIvvIvvIvvIvvIvvIvvIvvIvvIvvIvvIv
grey020.pgm             nvnvnvnvnvvvvnvnvvvnvvvnvnvnvvvnvvvnvnvnv
                        vvvvvvvvvvnvnvvvvnvvvnvvvvnvvvnvvnvvvvnvv
grey021.pgm             nnvnvnnnnnnvnvnvnnvnvnnvnnvnvnvnvnnvnnvnv
                        nnnnnnnnvnvnnnnnnnnnnnvnnnnnnnnnnnnnnvnnn
grey022.pgm             ononononnononnonnononnononononnnonnonnono
                        ononnnnonnonononnnonnonnonononoonnonnonnn
grey023.pgm             =o=o=o=o=o=o=o=o=o=o=o=n=o=oo=o=oo=o=oo=o
                        =o=o=n=o=o=o=o=o=o=o=o==o=o==o=o==o=o==o=
                        ==oo==o=o=o=o=o=o=o=o=oo=o=oo=o=oo=o=oo=o
grey024.pgm             ======================================
grey025.pgm             X==X=X=X=X=X=X=X=X=X=X=X=X=X=X=X=X=X=X=X=
                        =X=X==X==X==X==X==X==X==X==X==X==X==X==X=
grey026.pgm             XXX=XX=XX=XXX=XX=XXXX=XX=XX=XXXXX=XX=XX=X
                        X=XX=XXX=XX=XX=XX=X=X=X=XX=XX=X=X=X=XX=XX
                        XX=XX=X=XX=X=XX=XXX=XXXX=XX=XX=XXX=XX=XX=
grey027.pgm             XXXXZXXXZXXXXXXXXXXXXXXXXXXXZXXXXXXXZXXXX
                        ZXXZXXXXXXXZXXXXXXXXZXXZXXZXXXXXZXXZXXXXZ
                        XXXXXXXZXXZXXXZXXZXXXXXXXXXXXXXXXXXXXXXXX
grey028.pgm             ZZZXZXZXZZZXZZZXZZZXZZXZXZXZXZZZXZZXZXZXZ
                        ZZXZZXZZXZXZZXZZZXZZXZZZZXZZZZXZZXZZXZXZZ
                        ZXZZXZZXZZZXZZXZXZZXZZXZXZZXZXZZXZZXZZZZX
grey029.pgm             #Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#
                        ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
grey030.pgm             ##Z#Z###Z##Z#Z####Z#Z##Z##Z#Z###Z#Z###Z##
                        #Z###Z#Z##Z###Z#Z#Z###Z##Z#Z##Z#Z##Z#Z##Z
grey031.pgm             ######Z##Z##Z##Z##Z##Z##Z##Z##Z##Z##Z##Z#
                        #########################################
                        #####Z###################################
grey032.pgm             mmmWmmmWmmmWmmmWmmmWmmmWmmmWmmmWmmmWmmmWm
                        mmmmmmmm#mmm#mmm#mmm#mmm#mmm#mmm#mmm#mmm#
                        mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
grey033.pgm             WmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmW
                        mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
grey034.pgm             WmWWmWWmWWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWm
                        WmWmWmWmWmWmWWmWWmWWmWWmWWmWWmWWmWWmWWmWW
grey035.pgm             WWmWmWWWWmWWWWmWWWWmWWWWmWWWWmWWWWmWWWWmW
                        WWWWWWmWWWWmWWWWmWWWWmWWWWmWWWWmWWWWmWWWW
                        WWmWmWWWmWmWWmWmWWmWmWWmWmWWmWmWWmWmWWmWm
                        WWWWWWmWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
grey036.pgm             QWWQWWWWQWWWWQWWWWWQWWQWQWWWWWWWQWWWWWQWQ
                        QWWWWWQWWWWQWWWWQWWWWWWWWWWQWWQWWWWQWQWWW
grey037.pgm             QQWQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
                        QQQQQQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQW
                        QQQQWQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQ
grey038.pgm             QQQiQQQQQQiQQiQQiQQiQQQiQQQiQQQiQQQiQQQiQ
                        QiQQQQiQQQQQQQQQQQQQQQiQQQiQQQiQQQiQQQiQQ
                        QQQQQiQQQiQQiQQiQQiQQQQQQQQQQQQQQQQQQQQQQ
grey039.pgm             iQQiQiQiQQiQQiQQiQQQQiQQiQQiQQiQQiQQiQQiQ
                        QQiQQQQQiQQiQQiQQQiQiQQiQQiQQiQQiQQiQQiQQ
grey040.pgm             iQiiQiQiQiQiQiQiiQiQiiQiiQiQiQiiQiQiQiQiQ
                        iQiQiQiQiQiQiQiQiQiQiQiQiQiiQiQiQiiQiQiiQ
grey041.pgm             iiQiQiQiiiQiiQiiiiiiiiiiiiiiiiiiiiiiiiiii
                        iiiiiiiiiQiiiiiQiiQiQiiQiQiiQiQiiQiQiiQiQ
grey042.pgm             iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
grey043.pgm             liiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
                        iilililililililililililililililililililil
grey044.pgm             iliiliililiililililililililililililililil
                        lililililililiililililililililililililili
                        liilililililililiiliiliiliiliiliiliiliili
grey045.pgm             llililillllililillililililllllilililillll
                        lllllilllilllllllilllllillilillllillllili
grey046.pgm             llllllillillillillillillillillillillillil
                        lllllllllllllllllllllllllllllllllllllllll
grey047.pgm             IlIllIlIllIllIllIlIllIllIlIlIllIllIlIllIl
                        IlIlIlIllIlIlIlIlIlIlIlIlIlIllIlIlIlIlIlI
grey048.pgm             IIIIIIIIlIIlIIlIIlIIlIIlIIlIIlIIlIIlIIlII
                        IIIIlIIlIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
grey049.pgm             vIIIvIIIIIvlIIvlIvIIIIvIIIIIIvIIIIvIIIIII
                        IIIvlIvIIIIIvIIIvIIvIvlIvIIvIIIvIvlIvIIvI
                        vIvlvIIIvIvIIIvIIIIIIIIvlIvIIIvlIIIvlIvII
                        vIIIIIvIIIIIIvlIvIIvIIvlvIIIIvlvIIvIIvlII
grey050.pgm             IvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvIvI
grey051.pgm             vvvvIvvvvvIvIvvIvIvvvIvIvvvIvIvvvIvIvvvIv
                        vvIvvIvIvIvvvvIvvvIvIvvvIvIvvvIvIvvvIvIvv
                        vvvvIvvvvvvIvIvvIvvvvvIvvvvvIvvvvvIvvvvvI
grey052.pgm             vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
grey053.pgm             vvvvnvvvvvvnvvnvvnvvvvvnvvvvnvvvvvvvvvvvv
                        vnvvvvvvvnvvvvvvvvvvnvvvvvvvvvvnvvvvnvvnv
                        vvvvnvvnvvvvvnvvnvvvvvvnvvvnvvvvvvnvvvvvv
                        vnvvvvvvvvvvnvvvvvvnvvnvvvvvvvnvvvvvvvvnv
grey054.pgm             nvvnvvvvvnvvnvvnvvnvnvvnvvnvnvnvvnvvnvnvv
                        vnvvnvnnvvnvvnvvnvvnvvnvnvvnvvvvnvnvvnvvn
                        nvvnvvnvvnvnvvnvvnvvnvvnvvnvvnvnvvvnvvnvv
                        vnvvnvvvnvvvnvvnvnvnvvnvvnvvnvvnvnvnvnvvn
grey055.pgm             nvnvnvnvnvnvnvnvnvnvnvnvnvnvnvnvnvnvnvnvn
grey056.pgm             nvnnvnnnnvnnvnnvnnvnnvnnvnnvnnvnnvnnvnnvn
                        nnnvnnvnvnnvnnvnnvnnvnnvnnvnnvnnvnnvnnvnn
                        nvnnnvnnnvnnnvnnnnnvnnvnnvnnvnnvnnvnnvnnv
                        nnvnvnnvnnvnvnnvnvnnnvnnnnnvnnnnnvnnnnnvn
grey057.pgm             nnnnvnnvnnvnvnnvnvnnvnnvnvnnvnnvnnvnvnnvn
                        nnnvnnnnnvnnnnnnnnnnnnvnnnnnnnvnnnnnnnnnn
                        nnnnnnvnnnnnnnvnnvnnvnnnnnnvnnnnnvnnnvnnv
grey058.pgm             onnonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
                        nnnnnnnonnonnonnonnonnonnonnonnonnonnonno
                        nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
grey059.pgm             ononononnonnonnonnonnonnonnonnonnonnonnon
                        onononononononononononononononononononono
                        ononononononnonononononononononononononon
grey060.pgm             ooooooooooooooooooooooooooooooooooooooooo
grey061.pgm             2o2oo2oo2oooo2o2ooooo2ooooo2ooooo2ooooo2o
                        oooo2oo2oo2o2oooo2o2ooo2o2ooo2o2ooo2o2ooo
                        o2o2oo2ooooooo2o2ooo2o2ooo2o2ooo2o2ooo2o2
grey062.pgm             o2o2o2o2o2o2o2o2o2oo2oo2o2oo2oo2o2oo2oo2o
                        2o2o2o2o2oo2o2oo2o2o2o2o2o2o2o2o2o2o2o2o2
                        o2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o
                        2o2o2o2oo2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o2o
grey063.pgm             22222222222222222222222222222222222222222
                        22o2o22o22o22o22o22o22o22o22o22o22o22o22o
                        2o2222o22o22o22o22o22o22o22o22o22o22o22o2
grey064.pgm             22222222222222222222222222222222222222222
grey065.pgm             22222222S222222222222222S2222222222222222
                        S2S22S22222S222S22S22S22222S22S22S22S22So
grey066.pgm             S22S22S22SoS22S222222S22S22S22S22S22S22S2
                        S222S222S2S22SoS2S2S222S22SoS222SoS222SoS
grey067.pgm             S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S
                        S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S2S
grey068.pgm             S2SS2S2S2SSS2S2S2SS2S2S2S2S2SS2SS2S2SS2SS
                        SS2S2SS2SS2S2SSSS2S2SS2SSS2SS2SS2S2SS2S2S
                        S2SSS2SS2S2SS2S22SSS2SS2S2SS2S2S2SS2SS2SS
grey069.pgm             XS2SS2S2SS2SSSS2SS2SSSSSSS2XS2XSSSSSSSSSS
                        XSSSSSSSSSSSS2SSSSSSS2SS2SSSSSSS2SS2SS2SS
                        X2SS2SSS2SS2SSSS2SS2SSSSSSS2SS2SSSSSS2XSS
                        XSSSSS2SSSSSSSS2XSSSSS2SS2SSSSSSS2SS2XS2S
grey070.pgm             SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
grey071.pgm             XXSXSSXSSXSSXSSXSSXSSXSSXSSXSSXSSXSSXSSXS
                        XXXSXSXSXSXSXSXSXSXSXSXSXSXSXSXSXSXSXSXSX
grey072.pgm             XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
grey073.pgm             ZXXZXXXZXXXXZXXZXXXXXXXXXXZXXXXZXXXXXXXXX
                        XXZXXXXXXXZXXXXXXXXZXXZXZXXXXXZXXXZXXZXXZ
                        ZXXXXXZXXZXXXZXXZXXXXZXXXXXXZXXXXZXXXXXZX
                        XXXXZXXXXXXXXXXZXXXZXXXXXZXXXXXXZXXXZXXXX
grey074.pgm             ZXZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
                        ZXZXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
grey075.pgm             ZZXZZZZXZZZZZXZZZZZXZZZZZXZZZZZXZZZZZXZZZ
                        ZXZZXZXZZXZXZZZXZXZZZXZXZZZXZXZZZXZXZZZXZ
                        ZZZXZZZXZZXZZXZZXZZXZZXZZXZZXZZXZZXZZXZZX
                        ZZXZZXZZZXZZXZZXZZXZZXZZXZZXZZXZZXZZXZZXZ
grey076.pgm             ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
grey077.pgm             Z#ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
                        ZZZZZZZZZZZZZZ#ZZZZZZZZZZZZZZZ#ZZZZZZZZZZ
                        #ZZZZ#ZZZZZZ#ZZZZZ#ZZZZ#ZZZZ#ZZZZZ#ZZZZ#Z
                        ZZZZZZZZZ#ZZZZZZZZZZZZ#XZZZZZZZZZZZZZZ#XZ
grey078.pgm             ZZZZZZ#ZZ#Z#ZZ#Z#ZZ#Z#ZZ#Z#ZZ#Z#ZZ#Z#ZZ#Z
                        #Z#Z#ZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZ
                        ZZZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ
                        #Z#Z#ZZZZ#ZZZZ#ZZZ#ZZZZ#ZZZZ#ZZZZ#ZZZZ#ZZ
grey079.pgm             #Z#ZZ#ZZ#Z#Z#Z##Z##Z##Z##Z##Z##Z##Z##Z##Z
                        #ZZ#ZZ#ZZ#ZZZ#ZZZ#X#ZZZ#X#ZZZ#X#ZZZ#X#ZZZ
                        Z#ZZ#ZZ#ZZZ#ZZZ#ZZ#ZZ#ZZ#ZZ#ZZ#ZZ#ZZ#ZZ#Z
                        #ZZ#ZZ#ZZ#Z#Z#Z#Z#ZZ#ZZ#ZZ#ZZ#ZZ#ZZ#ZZ#ZZ
grey080.pgm             #Z#Z#ZZ#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z
                        #Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#
                        #Z#Z#Z#Z#Z#Z#Z#ZZ#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z#Z
grey081.pgm             ##Z##Z##Z##Z##Z##Z#Z##Z##Z#Z##Z#Z##Z#Z##Z
                        #Z##Z##Z##Z##Z#Z#Z##Z##Z#Z##Z#Z##Z#Z##Z##
                        ###Z##Z#Z#Z#Z##Z##Z##Z#Z##Z##Z##Z###Z##Z#
                        #Z#Z#Z###Z###Z##Z##Z#Z##Z##Z#Z#Z##Z#Z#Z##
grey082.pgm             ####Z#Z#####Z#####Z##Z##Z#########Z#Z##Z#
                        ##Z#####Z#Z###Z#Z###Z##Z##Z#Z#Z#Z####Z###
grey083.pgm             ####Z#######Z###Z###Z###Z###Z###Z###Z###Z
                        #########################################
                        ######Z##Z##Z######Z###Z###Z###Z###Z###Z#
                        ####Z###########Z########################
grey084.pgm             #########################################
grey085.pgm             m#m#m#m#m#m#m#m#m#m#m#m#m#m#m#m#m#m#m#m#m
grey086.pgm             mmmmmmmm#mmmmmmmm#mmmm#mmmmmm#mmmm#mmmmmm
                        mmmmmmm#mmmmm#m#mmmm#mmmm#m#mmmm#mmmm#m#m
                        mmmmmmmmmmmm#mmmmmmmmmmmmmmmmmmmmmmmmmmmm
grey087.pgm             BmmmmBmmmmBmmmmmmmmBmBmmmmBmmmBmmmmBmmmBm
                        mmmBmmmmBmmmBmBmmBmmmmmBmmmmmBmmBmmmmmBmm
grey088.pgm             mmmWmmmmmmmBmmWmmWmmmBmmBmBmBmmmBmmBmBmBm
                        BmBmmmBmWmmBmBmmmmmmBmBmBmmBmBmWmmWmmmBmm
                        mBmmBmBmmmBmmmmBmBmBmBmmBmBmmBmmmmmmmWmmB
                        WmmBmmBmmBmmWmBmmBmmBmmWmmmBmBmBmBmBmmmmB
grey089.pgm             mBmWmmmmmWmmBBBmmWmmBBBmmWmmBBBmmWmmBBBmm
                        WmWmmWmWmmmWmmWmBmmWmmWmBmmWmmWmBmmWmmWmB
                        mBmmWmmmWmWmmWmmBBmmBmmBmWmmmWmmBBmmBmmBm
grey090.pgm             BmWmWmWmBmBmWmWmmBmBmWmmWmmWmmWmBBWmBBWmW
                        WmBmWmmWmWmWmmBmWmWmWmBWmBBmWmBmWmmBBmmBm
grey091.pgm             WmWmWmWmWmWmWmWmBBBBBBBWmWmWmWmWmWmWmWmWm
                        WmWmBBBBBWmWmWmWmWmWmWmWmBBBBBBBBBBBBBBBB
                        WmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmWmW
grey092.pgm             WmWBWBWBBWmWmWWBWmWBWmWWmWWmWBWmWBWmWBWmW
                        WWmWmWmWBWBWBWmWmWBWmWBBWBBWmWmWBWmWBWmWB
grey093.pgm             WWBWWBWmWWBWmWWBWmWBWWBWWmWWmWWBWmWWmWWBW
                        WBWmWBWWBWmWWBWmWWWBWmWBWBWBWWmWWWWmWWmWW
                        WWmWWmWmWBWWmWBWWmWmWWBWmWWBWmWWmWmWWmWWm
grey094.pgm             WWBWmWBWmWBWmWBWmWBWmWBWmWBWmWBWmWBWmWBWm
                        WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
grey095.pgm             WWWWBWWWWWWWWWWWWBWWWBWWWWWWWBWWWBWWWWWWW
                        WWWWWWBWWmWWBWBWWWWWWWWWBWBWWWWWWWWWBWBWW
grey096.pgm             WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
grey097.pgm             QWWWWWWWWWWWWWWWWQWWQWWWWQWWWQWWQWWWWQWWW
                        QWWQWWQWWQWWQWQWWWWQWWWWQWWWWWWQWWWWQWWWW
                        QWWWWWWWWWWQWWWWWQWWWWWQWWWWQWWWWWWQWWWWQ
                        QWWQWWQWQWWWWWWWQWWWWQWWWWWQWWWQWWQWWWWQW
grey098.pgm             QQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQW
                        QWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQWQ
grey099.pgm             QQQQWQQWQQWQQQWQQWQQWQQWQQWQWQQWQQWQWQWQQ
                        QQWQQWQQWQQQWQQWQQWQQWQQWQQWQQWQQWQQQWQQW
grey100.pgm             QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
