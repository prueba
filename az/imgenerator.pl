use strict;

my $imagemagick;

foreach (0..100) {
    $imagemagick = sprintf "convert canvas.pgm -alpha Off -fill grey%s -colorize 100%% grey%03d.pgm", $_, $_;
    system($imagemagick);
}
